Insecttrap - controller software for the enclosed RaspberryPi-camera

The revolver suction trap has a attached Raspberry Pi with a camera aimed at the collection tube in use. This makes it possible to remotely document the functionality of the trap. The system is currently set up to use a mobile connection with high fault tolerance by taking pictures on a schedule and to upload these to a Amazon S3 bucket via mqtt whenever an internet connection is established. To control the Pi in case of a problem surfacing the Pi had a cron-job triggered every night downloading the latest controller program from git and running the launch script after a reboot. This turned out to be very reliable but cumbersome. 

A better way to access the camera would be to install a tunneling/mesh-app on the Pi e.g. https://www.zerotier.com/ or https://www.cloudflare.com/products/tunnel/ This would give direct access to all functioality of the Pi whenever needed. 

The camera has an attached Led-diode workning as a flash controlled by gpio-pin 18 so that photos can be taken during low light conditions.

All python script triggering the camera functions are called from cron. 



ToDo:
- Protect all network calls with handled timeouts : done in all py-scripts
- See to that all files gets sent upon network reconnect

All important stuff done!


Crontabs
root:
```text
# m  h   dom mon dow   command
  5  7   *   *   *     /sbin/shutdown -r now
```  
User [XXX]:
```text
# m    h   dom mon dow   command
  15   22  *   *   *     /home/XXX/insecttrap/bin/snap_night_picture.py
  15   12  *   *   *     /home/XXX/insecttrap/bin/snap_day_picture.py
  20   12  *   *   *     /home/XXX/insecttrap/bin/snap_night_picture.py
  0    */4 *   *   *     /home/XXX/insecttrap/bin/heartbeat.py
  */10 *   *   *   *     /home/XXX/insecttrap/bin/timestamp.py
  5    6   *   *   *     cd /home/XXX/insecttrap && git pull
```
