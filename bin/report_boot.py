#!/usr/bin/env python3
import insectenv
import subprocess
import boto3
import io
import time
import logging

def on_reboot():
    log_text = subprocess.check_output(["tail", "-100", insectenv.LOGFILE], universal_newlines=True)
    logging.info("*** Reboot report")
    time.sleep(60)
    
    if insectenv.internet_on():
        timestamp = time.strftime("%Y%m%d_%H%M%S", time.localtime(time.time()-60)) 
        f = io.open(insectenv.OUTPUT_PATH + "reboot.txt", "w", encoding="utf-8")
        f.write("REBOOT AT " + timestamp + "\n\n")
        text = subprocess.check_output("w", universal_newlines=True)
        f.write(text)
        text = subprocess.check_output(["curl", "-m 15", "ifconfig.co"], universal_newlines=True)
        f.write("\nIP: " + text)
        logging.info("    IP: " + text)
        f.write("\nLog:\n" + log_text)
        f.close()

        s3 = boto3.client('s3')
        bucket_name = 'camera.nacelle.projects.skallbe.net'
        s3.upload_file(insectenv.OUTPUT_PATH + "reboot.txt", bucket_name, 'reboot' + timestamp +'.txt')
        logging.info("    Report sent to S3")
    
if __name__ == "__main__":
    on_reboot() 
