#!/usr/bin/env python3
import insectenv
import os
import boto3
import logging

def send_pictures():
    if insectenv.internet_on():
        print("Network is up, sending files")
        s3 = boto3.client('s3')
        bucket_name = 'camera.nacelle.projects.skallbe.net'
        
        for file in os.listdir(insectenv.OUTPUT_PATH):
            if file.endswith(".jpg"):
                print('Sending: ' + file) #should be replaced with a call to real func.
                s3.upload_file(insectenv.OUTPUT_PATH + file, bucket_name, file)
                # now move the image to the archive
                print("Moving ", insectenv.OUTPUT_PATH + file, " to ", insectenv.ARCHIVE_PATH + file)
                os.rename(insectenv.OUTPUT_PATH + file, insectenv.ARCHIVE_PATH + file)
                logging.info("Photo sent " + file)
            else:
                print("Will not send " + file)
    else:
        print("Network is down, can not send files")
        logging.warning("Network is down, can not send files")


if __name__ == "__main__":
    send_pictures()
