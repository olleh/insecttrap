#!/usr/bin/env python3
import sys
import RPi.GPIO as GPIO
import time
import logging
import insectenv

class Ledpin18:
    def __init__(self):
        self.setup()
        
    def setup(self):
        print("LED Setup")
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(18,GPIO.OUT)
    
    def on(self):
        logging.info("LED on")
        GPIO.output(18,GPIO.HIGH)

    def off(self):
        logging.info("LED off")
        GPIO.output(18,GPIO.LOW)
        
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        print("LED off on teardown")
        GPIO.output(18,GPIO.LOW)
        
        
def main():
    #GPIO.setmode(GPIO.BCM)
    #GPIO.setwarnings(False)
    #GPIO.setup(18,GPIO.OUT)
    #print("LED on")
    #GPIO.output(18,GPIO.HIGH)
    #time.sleep(1)
    #print("LED off")
    #GPIO.output(18,GPIO.LOW)
    with Ledpin18() as led_flash:
        led_flash.on()
        time.sleep(1)
        led_flash.off()


if __name__ == "__main__":
    main()
