#!/usr/bin/env python3
import insectenv
from picamera import PiCamera
import sys
from time import sleep, strftime, localtime
from fractions import Fraction
import logging


def snap_picture(prefix):
    path = insectenv.OUTPUT_PATH
    name = prefix + "_" + strftime("%Y%m%d_%H%M%S", localtime()) + ".jpg"
    print('Path:', path+name)
    default_resolution = (1280, 720) #(1920, 1080)
    rotation_angle = 0 # 180 if upside down

    if prefix=='night':
        # Force sensor mode 3 (the long exposure mode), set
        # the framerate to 1/6fps, the shutter speed to 6s,
        # and ISO to 800 (for maximum gain)
        camera = PiCamera(
            resolution = default_resolution,
            framerate=Fraction(1, 6),
            sensor_mode=3)
        camera.rotation = rotation_angle
        camera.shutter_speed = 6000000
        camera.iso = 800
        # Give the camera a good long time to set gains and
        # measure AWB (you may wish to use fixed AWB instead)
        sleep(30)
        camera.exposure_mode = 'off'
        # Finally, capture an image with a 6s exposure. Due
        # to mode switching on the still port, this will take
        # longer than 6 seconds
        camera.capture(path+name)
    else:
        camera = PiCamera(resolution = default_resolution)
        camera.rotation = rotation_angle
        #camera.rotation = 180
        camera.start_preview()
        sleep(5)
        camera.capture(path+name)
        camera.stop_preview()
        
    logging.info("Photo " + name)

    
def main():
    if len(sys.argv) >= 2:
        prefix = sys.argv[1]
    else:
        prefix = 'day'
    snap_picture(prefix)
    
if __name__ == "__main__":
    main()
