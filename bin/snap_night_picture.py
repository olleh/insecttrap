#!/usr/bin/env python3
from snap_picture  import snap_picture
from send_pictures import send_pictures
from time import sleep
from ledpin import Ledpin18

def snap_night_picture():
    with Ledpin18() as led_flash:
            led_flash.on()
            snap_picture("flash")
            #time.sleep(1)
            led_flash.off()
            
    #snap_picture("night")
    sleep(5)
    send_pictures()


if __name__ == "__main__":
    snap_night_picture()
