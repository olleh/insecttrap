from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import sys
import logging
import subprocess
import time
import argparse
import os

BIN_PATH = '/home/olle/insecttrap/bin/'

# Custom MQTT message callback
def customCallback(client, userdata, message):
	print("Received a new message: ")
	print(message.payload)
	print("from topic: ")
	print(message.topic)
	print("--------------\n\n")

def snapNightPictureCallback(client, userdata, message):
	print("Received a new message: ")
	print(message.payload)
	print("from topic: ")
	print(message.topic)
	logging.info("IoT command: " + message.topic + " : " + message.payload)
	subprocess.call("python3 " + BIN_PATH + "snap_night_picture.py", shell=True)
	myAWSIoTMQTTClient.publish('InsectTrapCamera/success', "Took night picture!", 1)
	print("--------------\n\n")

def snapDayPictureCallback(client, userdata, message):
	print("Received a new message: ")
	print(message.payload)
	print("from topic: ")
	print(message.topic)
	logging.info("IoT command: " + message.topic + " : " + message.payload)
	subprocess.call("python3 " + BIN_PATH + "snap_day_picture.py", shell=True)
	myAWSIoTMQTTClient.publish('InsectTrapCamera/success', "Took day picture!", 1)
	print("--------------\n\n")

def heartbeatCallback(client, userdata, message):
	print("Received a new message: ")
	print(message.payload)
	print("from topic: ")
	print(message.topic)
	logging.info("IoT command: " + message.topic + " : " + message.payload)
	subprocess.call("python3 " + BIN_PATH + "heartbeat.py", shell=True)
	myAWSIoTMQTTClient.publish('InsectTrapCamera/success', "Sent heartbeat!", 1)
	print("--------------\n\n")

def gitPullCallback(client, userdata, message):
	print("Received a new message: ")
	print(message.payload)
	print("from topic: ")
	print(message.topic)
	logging.info("IoT command: " + message.topic + " : " + message.payload)
	os.chdir('/home/olle/insecttrap')
	text = subprocess.check_output("git pull", universal_newlines=True, shell=True)
	myAWSIoTMQTTClient.publish('InsectTrapCamera/success', "Git pull:" + text + "\n", 1)
	print("--------------\n\n")

# Read in command-line parameters
parser = argparse.ArgumentParser()
parser.add_argument("-e", "--endpoint", action="store", required=True, dest="host", help="Your AWS IoT custom endpoint")
parser.add_argument("-r", "--rootCA", action="store", required=True, dest="rootCAPath", help="Root CA file path")
parser.add_argument("-c", "--cert", action="store", dest="certificatePath", help="Certificate file path")
parser.add_argument("-k", "--key", action="store", dest="privateKeyPath", help="Private key file path")

args = parser.parse_args()
host = args.host
rootCAPath = args.rootCAPath
certificatePath = args.certificatePath
privateKeyPath = args.privateKeyPath
clientId = 'InsectTrapCamera'
topic = 'InsectTrapCamera/messages'

if (not args.certificatePath or not args.privateKeyPath):
	parser.error("Missing credentials for authentication.")
	exit(2)

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Init AWSIoTMQTTClient
myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
myAWSIoTMQTTClient.configureEndpoint(host, 8883)
myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 128, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect()
myAWSIoTMQTTClient.subscribe('InsectTrapCamera/messages', 1, customCallback)
myAWSIoTMQTTClient.subscribe('InsectTrapCamera/snap_night_picture', 1, snapNightPictureCallback)
myAWSIoTMQTTClient.subscribe('InsectTrapCamera/snap_day_picture', 1, snapDayPictureCallback)
myAWSIoTMQTTClient.subscribe('InsectTrapCamera/heartbeat', 1, heartbeatCallback)
myAWSIoTMQTTClient.subscribe('InsectTrapCamera/git_pull', 1, gitPullCallback)
time.sleep(2)

# Publish to the same topic in a loop forever
loopCount = 0
while True:
	myAWSIoTMQTTClient.publish(topic, "Camera waiting for orders " + str(loopCount), 1)
	loopCount += 1
	time.sleep(120)
