import urllib.request, urllib.error, urllib.parse
import logging

# ==== Golbal consts ====

BASE_PATH = '/home/olle/insecttrap/'

COMMAND_DIR = 'bin/'
COMMAND_PATH = BASE_PATH + COMMAND_DIR

OUTPUT_DIR = 'to_send/'
OUTPUT_PATH = BASE_PATH + OUTPUT_DIR

ARCHIVE_DIR = 'archive/'
ARCHIVE_PATH = BASE_PATH + ARCHIVE_DIR

LOGFILE = OUTPUT_PATH + 'insecttrap.log'

# ==== Logging ====

logging.basicConfig(format='%(levelname)s: %(asctime)s - %(message)s', filename=LOGFILE, level=logging.INFO)

# ==== Functions ====

def internet_on():
    for timeout in [1,5,10,15,30]:
        try:
            response=urllib.request.urlopen('http://google.com',timeout=timeout)
            return True
        except urllib.error.URLError as err: pass
    return False
