#!/usr/bin/env python3
import insectenv
import subprocess
import boto3
import io
import logging
import insectenv

def main():
    if insectenv.internet_on():
        f = io.open(insectenv.OUTPUT_PATH + "tmp.txt", "w", encoding="utf-8")
        text = subprocess.check_output("w", universal_newlines=True)
        f.write(text)
        text = subprocess.check_output(["curl", "-m 15", "ifconfig.co"], universal_newlines=True)
        f.write("\nExternal IP: " + text)
        text = subprocess.check_output("hostname -i",shell = True, universal_newlines=True)
        f.write("\nInternal IP: " + text)
        text = subprocess.check_output(["tail", "-5", insectenv.LOGFILE], universal_newlines=True)
        f.write("\nLog:\n" + text)
        f.close()

        s3 = boto3.client('s3')
        bucket_name = 'camera.nacelle.projects.skallbe.net'
        s3.upload_file(insectenv.OUTPUT_PATH + "tmp.txt", bucket_name, 'heartbeat.txt')
        logging.info("Heartbeat sent")
    
if __name__ == "__main__":
    main()
